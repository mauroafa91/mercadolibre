package co.moma.mercadolibre.model

import java.io.Serializable

class PaymentMethod: Serializable
{
    var id: String = ""
    var name : String = ""
    var payment_type_id : String = ""
    var status : String = ""
    var secure_thumbnail : String = ""
    var thumbnail : String = ""
    var deferred_capture : String = ""
    var settings : ArrayList<PaymentMethodSettings> = ArrayList()
    var additional_info_needed : ArrayList<String> = ArrayList()
    var min_allowed_amount : Double = 0.0
    var max_allowed_amount : Double = 0.0
    var accreditation_time : Int = 0
    var financial_institutions : ArrayList<String> = ArrayList()
    var processing_modes : ArrayList<String> = ArrayList()

    constructor()

    constructor(i_id: String, i_name: String, i_payment_type_id: String, i_status: String,
                i_secure_thumbnail: String, i_thumbnail: String, i_deferred_capture: String,
                i_settings: ArrayList<PaymentMethodSettings>, i_additional_info_needed: ArrayList<String>,
                i_min_allowed_amount: Double, i_max_allowed_amount: Double, i_accreditation_time: Int,
                i_financial_institutions: ArrayList<String>, i_processing_modes: ArrayList<String>) {
        this.id = i_id
        this.name = i_name
        this.payment_type_id = i_payment_type_id
        this.status = i_status
        this.secure_thumbnail = i_secure_thumbnail
        this.thumbnail = i_thumbnail
        this.deferred_capture = i_deferred_capture
        this.settings = i_settings
        this.additional_info_needed = i_additional_info_needed
        this.min_allowed_amount = i_min_allowed_amount
        this.max_allowed_amount = i_max_allowed_amount
        this.accreditation_time = i_accreditation_time
        this.financial_institutions = i_financial_institutions
        this.processing_modes = i_processing_modes
    }
}