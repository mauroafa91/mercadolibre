package co.moma.mercadolibre.model

import java.io.Serializable

class Bin: Serializable {
    var pattern: String ? = ""
    var installments_pattern: String ? = ""
    var exclusion_pattern: String ? = ""

    constructor()

    constructor(i_pattern: String, i_installments_pattern: String, i_exclusion_pattern: String) {
        this.pattern = i_pattern
        this.installments_pattern = i_installments_pattern
        this.exclusion_pattern = i_exclusion_pattern
    }
}