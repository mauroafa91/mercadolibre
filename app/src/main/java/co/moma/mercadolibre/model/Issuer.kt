package co.moma.mercadolibre.model

class Issuer
{
    var id: String ? = ""
    var name: String ? = ""
    var secure_thumbnail: String ? = ""
    var thumbnail: String ? = ""

    constructor()

    constructor(id: String?, name: String?, secure_thumbnail: String?, thumbnail: String?) {
        this.id = id
        this.name = name
        this.secure_thumbnail = secure_thumbnail
        this.thumbnail = thumbnail
    }
}