package co.moma.mercadolibre.model

import java.io.Serializable

class SecurityCode: Serializable {
    var length: Int ? = 0
    var card_location: String ? = ""
    var mode: String ? = ""

    constructor()

    constructor(i_length: Int, i_card_location: String, i_mode: String) {
        this.length = i_length
        this.card_location = i_card_location
        this.mode = i_mode
    }
}