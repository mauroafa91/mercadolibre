package co.moma.mercadolibre.model

import java.io.Serializable

class CardNumber: Serializable {
    var validation: String ? = ""
    var length: Int ? = 0

    constructor()

    constructor(i_validation: String, i_length: Int) {
        this.validation = i_validation
        this.length = i_length
    }
}