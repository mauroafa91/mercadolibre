package co.moma.mercadolibre.model

import java.io.Serializable

class PaymentMethodSettings: Serializable {
    var card_number: CardNumber ?= CardNumber()
    var bin: Bin ?= Bin()
    var security_code: SecurityCode ?= SecurityCode()

    constructor()

    constructor(i_card_number: CardNumber, i_bin: Bin, i_security_code: SecurityCode) {
        this.card_number = i_card_number
        this.bin = i_bin
        this.security_code = i_security_code
    }
}