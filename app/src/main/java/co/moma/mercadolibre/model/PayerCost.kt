package co.moma.mercadolibre.model

import java.io.Serializable

class PayerCost: Serializable {
    var installments: Int ? = 0
    var installment_rate: Double ? = 0.0
    var discount_rate: Int ? = 0
    var labels: ArrayList<String> ? = ArrayList()
    var installment_rate_collector: ArrayList<String> ? = ArrayList()
    var min_allowed_amount: Int ? = 0
    var max_allowed_amount: Int ? = 0
    var recommended_message: String ? = ""
    var installment_amount: Double ? = 0.0
    var total_amount: Double ? = 0.0

    constructor()

    constructor(installments: Int?, installment_rate: Double?, discount_rate: Int?, labels: ArrayList<String>?, installment_rate_collector: ArrayList<String>?, min_allowed_amount: Int?, max_allowed_amount: Int?, recommended_message: String?, installment_amount: Double?, total_amount: Double?) {
        this.installments = installments
        this.installment_rate = installment_rate
        this.discount_rate = discount_rate
        this.labels = labels
        this.installment_rate_collector = installment_rate_collector
        this.min_allowed_amount = min_allowed_amount
        this.max_allowed_amount = max_allowed_amount
        this.recommended_message = recommended_message
        this.installment_amount = installment_amount
        this.total_amount = total_amount
    }
}