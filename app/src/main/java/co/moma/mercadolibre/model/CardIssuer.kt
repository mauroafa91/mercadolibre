package co.moma.mercadolibre.model

import java.io.Serializable

class CardIssuer: Serializable {
    var id: String ? = ""
    var name: String ? = ""
    var secure_thumbnail: String ? = ""
    var thumbnail: String ? = ""
    var processing_mode: String ? = ""
    var merchant_account_id: Int ? = null

    constructor()

    constructor(i_id: String, i_name: String, i_secure_thumbnail: String, i_thumbnail: String,
                i_processing_mode: String, i_merchant_account_id: Int) {
        this.id = i_id
        this.name = i_name
        this.secure_thumbnail = i_secure_thumbnail
        this.thumbnail = i_thumbnail
        this.processing_mode = i_processing_mode
        this.merchant_account_id = i_merchant_account_id
    }
}