package co.moma.mercadolibre.model

class Installments
{
    var payment_method_id: String ? = ""
    var payment_type_id: String ? = ""
    var issuer: Issuer ? = Issuer()
    var processing_mode: String ? = ""
    var merchant_account_id: Int ? = null
    var payer_costs: ArrayList<PayerCost> ? = ArrayList()

    constructor()

    constructor(payment_method_id: String?, payment_type_id: String?, issuer: Issuer?, processing_mode: String?, merchant_account_id: Int?, payer_costs: ArrayList<PayerCost>?) {
        this.payment_method_id = payment_method_id
        this.payment_type_id = payment_type_id
        this.issuer = issuer
        this.processing_mode = processing_mode
        this.merchant_account_id = merchant_account_id
        this.payer_costs = payer_costs
    }


}