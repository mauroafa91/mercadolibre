package co.moma.mercadolibre.activities

import android.content.Intent
import android.graphics.drawable.PictureDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import co.moma.mercadolibre.adapters.InstallmentsAdapter
import co.moma.mercadolibre.model.CardIssuer
import co.moma.mercadolibre.model.Installments
import co.moma.mercadolibre.model.PayerCost
import co.moma.mercadolibre.model.PaymentMethod
import co.moma.mercadolibre.R
import co.moma.mercadolibre.utils.GlideApp
import co.moma.mercadolibre.utils.SvgSoftwareLayerSetter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_payment_activiy.*

class PaymentActivity : BaseActivity() {

    var TAG = "BankSelection"
    var amount: String = ""
    var paymentMethod: PaymentMethod = PaymentMethod()
    var cardIssuer: CardIssuer = CardIssuer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_activiy)

        if(intent.extras != null) {
            amount = intent.getStringExtra("sAmount")
            paymentMethod = intent.getSerializableExtra("PaymentMethod") as PaymentMethod
            cardIssuer = intent.getSerializableExtra("Bank") as CardIssuer
            Log.d(TAG, String.format("Initiated activity with amount %s and PaymentMethod - %s and Issuer - %s",
                    amount, paymentMethod.name, cardIssuer.name))
        }

        sp_payments.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                populateRecommendedMessage(sp_payments.selectedItem as PayerCost)
            }
        }

        bt_continue.setOnClickListener {
            val intent = Intent(this, Main::class.java)
            intent.putExtra("Amount", amount)
            intent.putExtra("PaymentMethod", paymentMethod)
            intent.putExtra("Bank", cardIssuer)
            intent.putExtra("PayerCost", sp_payments.selectedItem as PayerCost)
            startActivity(intent)
        }

        compositeDisposable.add(paymentAPI.getInstallments(amount, paymentMethod.id, cardIssuer.id!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{paymentMethod->displayData(paymentMethod)}
        )
    }

    fun displayData(installments: ArrayList<Installments>) {
        val spinnerAdapter = InstallmentsAdapter(applicationContext, installments[0].payer_costs!!)
        sp_payments.adapter = spinnerAdapter
        populateRecommendedMessage(installments[0].payer_costs!![0])

        val requestBuilder = GlideApp.with(applicationContext)
                .`as`(PictureDrawable::class.java)
                .error(R.drawable.ic_baseline_credit_card_24px)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(SvgSoftwareLayerSetter())

        val issuer = installments[0].issuer

        if(issuer!!.thumbnail!!.contains(".svg")) {
            requestBuilder.load(issuer.thumbnail).into(imgPayment)
        }
        else {
            Glide.with(applicationContext).load(issuer.thumbnail).into(imgPayment)
        }
    }

    fun populateRecommendedMessage(payerCost: PayerCost) {
        tv_reommended_message.text = payerCost.recommended_message
    }
}