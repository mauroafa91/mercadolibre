package co.moma.mercadolibre.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import co.moma.mercadolibre.adapters.SpinnerAdapter
import co.moma.mercadolibre.model.CardIssuer
import co.moma.mercadolibre.model.PaymentMethod
import co.moma.mercadolibre.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_card_selection.*

class BankSelection : BaseActivity() {

    var TAG = "BankSelection"
    var amount: String = ""
    var paymentMethod: PaymentMethod = PaymentMethod()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_selection)

        if(intent.extras != null) {
            amount = intent.getStringExtra("sAmount")
            paymentMethod = intent.getSerializableExtra("PaymentMethod") as PaymentMethod
            Log.d(TAG, String.format("Initiated activity with amount %s and PaymentMethod - %s", amount, paymentMethod.name))
        }

        compositeDisposable.add(paymentAPI.getCardIssuer(paymentMethod.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{paymentMethod->displayData(paymentMethod)}
        )

        bt_continue_card.setOnClickListener {
            val intent = Intent(this, PaymentActivity::class.java)
            intent.putExtra("sAmount", amount)
            intent.putExtra("PaymentMethod", paymentMethod)
            intent.putExtra("Bank", sp_credit_cards.selectedItem as CardIssuer)
            startActivity(intent)
        }
    }

    fun displayData(alCardIssuer: ArrayList<CardIssuer>) {

        val spinnerAdapter = SpinnerAdapter(applicationContext, alCardIssuer)
        sp_credit_cards.adapter = spinnerAdapter
    }
}
