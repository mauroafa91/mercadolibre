package co.moma.mercadolibre.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import co.moma.mercadolibre.adapters.SpinnerAdapter
import co.moma.mercadolibre.model.PaymentMethod
import co.moma.mercadolibre.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_card_selection.*

class CardSelection : BaseActivity() {

    private var sAmount = ""
    private var TAG = "CardSelection"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_selection)

        if(intent.extras != null) {
            sAmount = intent.getStringExtra("Amount")
            Log.d(TAG, String.format("Got amount %s from previous activity", sAmount))
        }

        compositeDisposable.add(paymentAPI.paymentMethods
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{paymentMethod->displayData(paymentMethod)}
        )

        bt_continue_card.setOnClickListener {

            val paymentMethod = sp_credit_cards.selectedItem as PaymentMethod
            if(sAmount.toDouble() <= paymentMethod.min_allowed_amount ||
                    sAmount.toDouble() >= paymentMethod.max_allowed_amount) {
                Snackbar.make(cl_card_selection, String.format(resources.getString(R.string.amount_warning),
                        paymentMethod.min_allowed_amount.toString(), paymentMethod.max_allowed_amount.toString()),
                        Snackbar.LENGTH_LONG).show()
            }
            else {
                val intent = Intent(this, BankSelection::class.java)
                intent.putExtra("sAmount", sAmount)
                intent.putExtra("PaymentMethod", sp_credit_cards.selectedItem as PaymentMethod)
                startActivity(intent)
            }
        }
    }

    fun displayData(paymentMethods: ArrayList<PaymentMethod>) {
        val spinnerAdapter = SpinnerAdapter(applicationContext, paymentMethods)
        sp_credit_cards.adapter = spinnerAdapter
    }
}
