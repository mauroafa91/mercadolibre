package co.moma.mercadolibre.activities

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.ViewGroup
import co.moma.mercadolibre.`interface`.IPaymentInterface
import co.moma.mercadolibre.R
import co.moma.mercadolibre.utils.RetrofitClient
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.content_home.*

open class BaseActivity: AppCompatActivity() {
    internal lateinit var paymentAPI: IPaymentInterface
    internal lateinit var compositeDisposable: CompositeDisposable

    private var TAG = "BaseActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.content_home)

        val retroFit = RetrofitClient.instance
        paymentAPI = retroFit.create(IPaymentInterface::class.java)
        compositeDisposable = CompositeDisposable()
    }

    override fun setContentView(layoutResID: Int) {
        if (frame != null) {
            val inflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val lp = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT)
            val stubView = inflater.inflate(layoutResID, frame, false)
            frame.addView(stubView, lp)
        }
    }
}