package co.moma.mercadolibre.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.transition.Slide
import android.transition.TransitionManager
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import co.moma.mercadolibre.model.CardIssuer
import co.moma.mercadolibre.model.PayerCost
import co.moma.mercadolibre.model.PaymentMethod
import co.moma.mercadolibre.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_home.*

class Main : BaseActivity() {
    private var TAG = "Main"
    var amount: String = ""
    var paymentMethod: PaymentMethod = PaymentMethod()
    var cardIssuer: CardIssuer = CardIssuer()
    var payerCost: PayerCost = PayerCost()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(intent.extras != null) {
            amount = intent.getStringExtra("Amount")
            paymentMethod = intent.getSerializableExtra("PaymentMethod") as PaymentMethod
            cardIssuer = intent.getSerializableExtra("Bank") as CardIssuer
            payerCost = intent.getSerializableExtra("PayerCost") as PayerCost

            showPopUp()

            Log.d(TAG, String.format("Initiated activity with amount %s and PaymentMethod - %s and Issuer - %s with the payments: %s",
                    amount, paymentMethod.name, cardIssuer.name, payerCost.installments))
        }

        bt_continue.setOnClickListener {
            val intent = Intent(this, CardSelection::class.java)
            intent.putExtra("Amount", et_amount.text.toString())
            startActivity(intent)
        }
    }

    fun showPopUp() {
        // Initialize a new layout inflater instance
        val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        // Inflate a custom view using layout inflater
        val view = inflater.inflate(R.layout.payment_summary,null)

        // Initialize a new instance of popup window
        val popupWindow = PopupWindow(
                view, // Custom view to show in popup window
                LinearLayout.LayoutParams.WRAP_CONTENT, // Width of popup window
                LinearLayout.LayoutParams.WRAP_CONTENT // Window height
        )
        popupWindow.elevation = 10.0F

        // If API level 23 or higher then execute the code
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            // Create a new slide animation for popup window enter transition
            val slideIn = Slide()
            slideIn.slideEdge = Gravity.TOP
            popupWindow.enterTransition = slideIn

            // Slide animation for popup window exit transition
            val slideOut = Slide()
            slideOut.slideEdge = Gravity.END
            popupWindow.exitTransition = slideOut

        }


        val tvAmount = view.findViewById<TextView>(R.id.tv_amount)
        val tvPaymentMethod = view.findViewById<TextView>(R.id.tv_payment_method)
        val tvCardIssuer = view.findViewById<TextView>(R.id.tv_card_issuer)
        val tvPayerCost = view.findViewById<TextView>(R.id.tv_payer_cost)
        val btOK = view.findViewById<Button>(R.id.bt_ok)

        btOK.setOnClickListener{
            popupWindow.dismiss()
        }
        popupWindow.setOnDismissListener {
            //goHome()
        }

        TransitionManager.beginDelayedTransition(cl_container)

        cl_container.post {
            popupWindow.showAtLocation(
                    cl_container,
                    Gravity.CENTER,
                    0,
                    0
            )

            tvAmount.text = amount
            tvPaymentMethod.text = paymentMethod.name
            tvCardIssuer.text = cardIssuer.name
            tvPayerCost.text = payerCost.recommended_message

            popupWindow.contentView.findFocus()
            popupWindow.isFocusable = true
            popupWindow.update()
        }
    }
}
