package co.moma.mercadolibre.adapters

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import co.moma.mercadolibre.model.PayerCost
import co.moma.mercadolibre.R
import co.moma.mercadolibre.utils.GlideApp
import co.moma.mercadolibre.utils.SvgSoftwareLayerSetter
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class InstallmentsAdapter(val context: Context, val alPayerCost: ArrayList<PayerCost>) : BaseAdapter() {

    val mInflater: LayoutInflater = LayoutInflater.from(context)
    var requestBuilder: RequestBuilder<PictureDrawable>? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ItemRowHolder

        requestBuilder = GlideApp.with(parent!!.context)
                .`as`(PictureDrawable::class.java)
                //.error(R.drawable.ic_account_circle_black_48px)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(SvgSoftwareLayerSetter())

        if(convertView == null) {
            view = mInflater.inflate(R.layout.spinner_payments, parent, false)
            holder = ItemRowHolder(view)
            view.tag = holder
        }
        else {
            view = convertView
            holder = view.tag as ItemRowHolder
        }

        val params = view.layoutParams
        params.height = 60
        view.layoutParams =
                params

        val paymentText = if(alPayerCost[position].installments!! > 1) "cuotas" else "cuota"
        var paymentLabel = ""

        alPayerCost[position].labels!!.forEach { paymentLabel += it }

        holder.label.text = String.format("%s %s (%s%%)",
                alPayerCost[position].installments,
                paymentText,
                alPayerCost[position].installment_rate
                )

        return view
    }

    override fun getItem(position: Int): Any {
        return alPayerCost[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return alPayerCost.size
    }

    private class ItemRowHolder(row: View?) {
        val label: TextView
        init {
            this.label = row!!.findViewById(R.id.txtDropDownLabel) as TextView
        }
    }
}