package co.moma.mercadolibre.adapters

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import co.moma.mercadolibre.model.CardIssuer
import co.moma.mercadolibre.model.Issuer
import co.moma.mercadolibre.model.PaymentMethod
import co.moma.mercadolibre.R
import co.moma.mercadolibre.utils.GlideApp
import co.moma.mercadolibre.utils.SvgSoftwareLayerSetter
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class SpinnerAdapter(val context: Context, val alAny: ArrayList<*>) : BaseAdapter() {

    val mInflater: LayoutInflater = LayoutInflater.from(context)
    var requestBuilder: RequestBuilder<PictureDrawable>? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ItemRowHolder
        var name = ""
        var thumbnail = ""

        requestBuilder = GlideApp.with(parent!!.context)
                .`as`(PictureDrawable::class.java)
                .error(R.drawable.ic_baseline_credit_card_24px)
                .transition(DrawableTransitionOptions.withCrossFade())
                .listener(SvgSoftwareLayerSetter())

        if(convertView == null) {
            view = mInflater.inflate(R.layout.spinner_credit_card, parent, false)
            holder = ItemRowHolder(view)
            view.tag = holder
        }
        else {
            view = convertView
            holder = view.tag as ItemRowHolder
        }

        val params = view.layoutParams
        params.height = 60
        view.layoutParams = params

        var currentType: Any ?= null

        when(alAny[position]) {
            is PaymentMethod -> {
                currentType = alAny[position] as PaymentMethod
                name = currentType.name
                thumbnail = currentType.thumbnail
            }
            is CardIssuer  -> {
                currentType = alAny[position] as CardIssuer
                name = currentType.name!!
                thumbnail = currentType.thumbnail!!
            }
            is Issuer -> {
                currentType = alAny[position] as Issuer
                name = currentType.name!!
                thumbnail = currentType.thumbnail!!
            }
        }

        holder.label.text = name

        //Set the image with Glide using the URI
        if(thumbnail.contains(".svg")) {
            requestBuilder!!.load(thumbnail).into(holder.image)
        }
        else {
            Glide.with(context).load(thumbnail).into(holder.image)
        }

        return view
    }

    override fun getItem(position: Int): Any {
        return alAny[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return alAny.size
    }

    private class ItemRowHolder(row: View?) {

        val label: TextView
        val image: ImageView

        init {
            this.label = row!!.findViewById(R.id.txtDropDownLabel) as TextView
            this.image = row.findViewById(R.id.imgDropDownMenuIcon) as ImageView
        }
    }
}