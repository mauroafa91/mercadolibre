package co.moma.mercadolibre.`interface`

import co.moma.mercadolibre.model.CardIssuer
import co.moma.mercadolibre.model.Installments
import co.moma.mercadolibre.model.PaymentMethod
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import kotlin.collections.ArrayList

interface IPaymentInterface {

    @get:GET("payment_methods?public_key=444a9ef5-8a6b-429f-abdf-587639155d88")
    val paymentMethods: Observable<ArrayList<PaymentMethod>>

    @GET("payment_methods/card_issuers?public_key=444a9ef5-8a6b-429f-abdf-587639155d88")
    fun getCardIssuer(@Query("payment_method_id") payment_method_id: String):Observable<ArrayList<CardIssuer>>

    @GET("payment_methods/installments?public_key=444a9ef5-8a6b-429f-abdf-587639155d88")
    fun getInstallments(@Query("amount") amount: String, @Query("payment_method_id") payment_method_id: String, @Query("issuer.id") issuer_id: String):Observable<ArrayList<Installments>>
}